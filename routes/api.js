// TODO 2: SETUP ROUTING (ROUTER)
const express = require("express");

const PatientsController = require("../controllers/PatientsController.js");

const router = express.Router();

router.get("/", (req, res) => {
    res.send("hello world");
});

router.get("/patients", PatientsController.index);
router.get("/patients/:id", PatientsController.show);
router.post("/patients", PatientsController.validate('store'),PatientsController.store);
router.put("/patients/:id", PatientsController.update);
router.delete("/patients/:id", PatientsController.destroy);
router.get("/patients/search/:name", PatientsController.search);
router.get("/patients/status/positive", PatientsController.positive);
router.get("/patients/status/recovered", PatientsController.recovered);
router.get("/patients/status/dead", PatientsController.dead);

module.exports = router;